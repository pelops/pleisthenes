Pleisthenes is a weather microservice (connects to a weather site and provides e.g. local sun radiation).

![Pelops Overview](img/Microservice Overview.png)

```Pleisthenes``` is part of the collection of mqtt based microservices [pelops](https://gitlab.com/pelops). An overview
on the microservice architecture and examples can be found at (http://gitlab.com/pelops/pelops).
